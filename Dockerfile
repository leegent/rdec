FROM python:3.6.9

ENV PYTHONBUFFERED=1

WORKDIR /app
ADD . .
RUN pip install -r requirements.txt
RUN python manage.py collectstatic --noinput

CMD ["gunicorn", "rdecsite.wsgi", "--log-file", "-", "-b", "0.0.0.0:80", "--access-logfile", "-"]
EXPOSE 80
